{include file='./head.tpl'}
<div class="col-sm-6 col-sm-offset-3">
	<div class="alert alert-info">
		{if $telegram_id != false}
			<strong>你已绑定Telegram帐号</strong>
			<br>
			Telegram UserID: {$telegram_id}
		{else}
			你目前未绑定Telegram帐号
		{/if}
	</div>
</div>

<div class="col-sm-6 col-sm-offset-3">
	<a class="btn btn-success" href="index.php?m=TelegramBot&action=bind">
		修改
	</a>
	<a class="btn btn-primary" href="clientarea.php">
		返回
	</a>
</div>

{include file='./foot.tpl'}
{include file='./head.tpl'}
<div class="col-sm-6 col-sm-offset-3">
	<div class="alert alert-warning">
		{if $telegram_id != false}
			<strong>你已绑定Telegram帐号</strong>
			<br>
			Telegram UserID: {$telegram_id}
			<br>
			<br>
			请加入 <a href="https://t.me/{$params['subscribe_chatid']}">@{$params['subscribe_chatid']}</a> 频道
		{else}
			你目前未绑定Telegram帐号
		{/if}
	</div>
</div>

<div class="col-sm-6 col-sm-offset-3">
	<a class="btn btn-success" href="https://t.me/{$params['subscribe_chatid']}">
		加入频道
	</a>

	<a class="btn btn-info" href="index.php?m=TelegramBot&action=change">
		修改绑定
	</a>

	<a class="btn btn-primary" href="clientarea.php">
		返回
	</a>
</div>

{include file='./foot.tpl'}
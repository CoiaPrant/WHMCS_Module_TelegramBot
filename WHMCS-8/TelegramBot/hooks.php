<?php
require_once(__DIR__ . '/func.php');

use WHMCS\Database\Capsule;

add_hook('TicketOpen', 1, function ($vars) {
    $config = TelegramBot_GetConfigModule();

    if ($config['ticket_auto']) {
        $GLOBALS['telegram']['skip_admin_reply_hook'] = true;

        localAPI("AddTicketReply", [
            'ticketid' => $vars['ticketid'],
            'adminusername' => '系统消息 (System Message)',
            'message' => $config['ticket_automsg'],
            'status' => 'Open',
            'markdown' => false,
        ]);
    }

    if ($config['ticket']) {
        $result = TelegramBot_send(
            $config['token'],
            "createForumTopic",
            [
                "chat_id" =>  $config['ticket_chatid'],
                "name" => "[WHMCS Ticket #" . $vars['ticketmask'] . "] " . html_entity_decode($vars['subject']),
            ]
        );

        $json = json_decode($result, true);
        if (!$json['ok']) {
            return;
        }

        $topic_id = $json['result']['message_thread_id'];
        Capsule::table("mod_TelegramBot_Ticket")->insert(["ticket_id" => $vars['ticketid'], "topic_id" => $topic_id, "is_closed" => false]);

        $result = TelegramBot_send(
            $config['token'],
            "sendMessage",
            [
                "chat_id" =>  $config['ticket_chatid'],
                "message_thread_id" =>  $topic_id,
                "text" => '<strong>工单详细</strong>' . PHP_EOL . '部门: ' . $vars['deptname'] .  PHP_EOL . "优先级: " . $vars['priority'] . PHP_EOL . "链接: " . TelegramBot_GetSystemURL() . TelegramBot_GetAdminPath() . '/supporttickets.php?action=view&id=' . $vars['ticketid'],
                "parse_mode" => "HTML",
            ]
        );

        $json = json_decode($result, true);
        if (!$json['ok']) {
            return;
        }

        TelegramBot_send(
            $config['token'],
            "pinChatMessage",
            [
                "chat_id" =>  $config['ticket_chatid'],
                "message_id" =>  $json['result']['message_id'],
            ]
        );

        TelegramBot_send(
            $config['token'],
            "sendMessage",
            [
                "chat_id" =>  $config['ticket_chatid'],
                "message_thread_id" => $topic_id,
                "text" => html_entity_decode($vars['message']),
            ]
        );

        $attachments = Capsule::table('tbltickets')->where('id', $vars["ticketid"])->first()->attachment;
        if (!empty($attachments)) {
            $attachments = explode("|", $attachments);
            foreach ($attachments as $attachment) {
                $storage = Storage::ticketAttachments();
                if (!$storage->has($attachment)) {
                    continue;
                }

                TelegramBot_send(
                    $config['token'],
                    "sendDocument",
                    [
                        "chat_id" =>  $config['ticket_chatid'],
                        "message_thread_id" => $topic_id,
                    ],
                    [
                        "document" => [
                            'name' => substr($attachment, 7),
                            'content' => $storage->read($attachment),
                        ],
                    ]
                );
            }
        }
    }
});

add_hook('TicketAdminReply', 1, function ($vars) {
    $config = TelegramBot_GetConfigModule();

    if ($GLOBALS['telegram']['skip_admin_reply_hook'] == true) {
        return;
    }

    if ($config['ticket']) {
        $ticket = Capsule::table("mod_TelegramBot_Ticket")->where("ticket_id", $vars["ticketid"])->first();
        if ($ticket == null) {
            return;
        }

        if (!empty($vars['message'])) {
            TelegramBot_send(
                $config['token'],
                "sendMessage",
                [
                    "chat_id" =>  $config['ticket_chatid'],
                    "message_thread_id" => $ticket->topic_id,
                    "text" => "<strong>管理员(" . $vars['admin'] . ")的回复:</strong>" . PHP_EOL  . $vars['message'],
                    "parse_mode" => "HTML",
                ]
            );
        }

        $attachments = Capsule::table('tblticketreplies')->where('id', $vars["replyid"])->first()->attachment;
        if (!empty($attachments)) {
            $attachments = explode("|", $attachments);
            foreach ($attachments as $attachment) {
                $storage = Storage::ticketAttachments();
                if (!$storage->has($attachment)) {
                    continue;
                }

                TelegramBot_send(
                    $config['token'],
                    "sendDocument",
                    [
                        "chat_id" =>  $config['ticket_chatid'],
                        "message_thread_id" => $ticket->topic_id,
                        "caption" => "<strong>管理员(" . $vars['admin'] . ")的回复</strong>",
                        "parse_mode" => "HTML",

                    ],
                    [
                        "document" => [
                            'name' => substr($attachment, 7),
                            'content' => $storage->read($attachment),
                        ],
                    ]
                );
            }
        }
    }
});

add_hook('TicketUserReply', 1, function ($vars) {
    $config = TelegramBot_GetConfigModule();

    if ($config['ticket']) {
        $ticket = Capsule::table("mod_TelegramBot_Ticket")->where("ticket_id", $vars["ticketid"])->first();
        if ($ticket == null) {
            return;
        }

        if ((bool)$ticket->is_closed) {
            Capsule::table("mod_TelegramBot_Ticket")->where("ticket_id", $vars["ticketid"])->update(["is_closed" => false]);

            TelegramBot_send(
                $config['token'],
                "reopenForumTopic",
                [
                    "chat_id" =>  $config['ticket_chatid'],
                    "message_thread_id" => $ticket->topic_id
                ]
            );

            TelegramBot_send(
                $config['token'],
                "sendMessage",
                [
                    "chat_id" =>  $config['ticket_chatid'],
                    "message_thread_id" =>  $ticket->topic_id,
                    "text" => '<strong>用户重新打开了工单</strong>',
                    "parse_mode" => "HTML",
                ]
            );
        }

        TelegramBot_send(
            $config['token'],
            "sendMessage",
            [
                "chat_id" =>  $config['ticket_chatid'],
                "message_thread_id" => $ticket->topic_id,
                "text" => html_entity_decode($vars['message']),
            ]
        );

        $attachments = Capsule::table('tblticketreplies')->where('id', $vars["replyid"])->first()->attachment;
        if (!empty($attachments)) {
            $attachments = explode("|", $attachments);
            foreach ($attachments as $attachment) {
                $storage = Storage::ticketAttachments();
                if (!$storage->has($attachment)) {
                    continue;
                }

                TelegramBot_send(
                    $config['token'],
                    "sendDocument",
                    [
                        "chat_id" =>  $config['ticket_chatid'],
                        "message_thread_id" => $ticket->topic_id,
                    ],
                    [
                        "document" => [
                            'name' => substr($attachment, 7),
                            'content' => $storage->read($attachment),
                        ],
                    ]
                );
            }
        }
    }
});

add_hook('TicketClose', 1, function ($vars) {
    $config = TelegramBot_GetConfigModule();

    if ($config['ticket']) {
        $ticket = Capsule::table("mod_TelegramBot_Ticket")->where("ticket_id", $vars["ticketid"])->first();
        if ($ticket == null) {
            return;
        }

        TelegramBot_send(
            $config['token'],
            "sendMessage",
            [
                "chat_id" =>  $config['ticket_chatid'],
                "message_thread_id" => $ticket->topic_id,
                "text" => "<strong>工单已关闭</strong>",
                "parse_mode" => "HTML"
            ]
        );

        Capsule::table("mod_TelegramBot_Ticket")->where("ticket_id", $vars["ticketid"])->update(["is_closed" => true]);

        TelegramBot_send(
            $config['token'],
            "closeForumTopic",
            [
                "chat_id" =>  $config['ticket_chatid'],
                "message_thread_id" => $ticket->topic_id
            ]
        );
    }
});

add_hook('TicketDelete', 1, function ($vars) {
    $config = TelegramBot_GetConfigModule();

    if ($config['ticket']) {
        $ticket = Capsule::table("mod_TelegramBot_Ticket")->where("ticket_id", $vars["ticketId"])->first();
        if ($ticket == null) {
            return;
        }

        Capsule::table("mod_TelegramBot_Ticket")->where("ticket_id", $vars["ticketId"])->delete();

        TelegramBot_send(
            $config['token'],
            "deleteForumTopic",
            [
                "chat_id" =>  $config['ticket_chatid'],
                "message_thread_id" => $ticket->topic_id
            ]
        );
    }
});

add_hook('ProductEdit', 1, function ($vars) {
    $config = TelegramBot_GetConfigModule();

    TelegramBot_Stock_Upgrade();
    if (!$config['stock_hook'] || !$config['enable_channel']) {
        return;
    }

    if ($vars['hidden']) {
        return;
    }

    if ($vars['stockcontrol'] && $vars['qty'] <= 0) {
        return;
    }

    if (!$vars['stockcontrol']) {
        $sc = '[库存:大量]';
    } else {
        $sc = '[库存:' . $vars['qty'] . ']';
    }

    if ($vars['paytype'] == 'free') {
        $pricing = '免费';
    } else if ($vars['paytype'] == 'onetime') {
        $pri =  Capsule::table('tblpricing')->where('relid', $vars['pid'])->get()[0];
        $curr = Capsule::table('tblcurrencies')->where('id', $pri->currency)->get()[0];
        $pricing = $pri->msetupfee + $pri->monthly;
        $pricing = $pricing . ' ' . $curr->code . '/一次性';
    } else {
        $pri =  Capsule::table('tblpricing')->where('relid', $vars['pid'])->get()[0];
        $curr = Capsule::table('tblcurrencies')->where('id', $pri->currency)->get()[0];
        if ($pri->msetupfee != 0.00) {
            $pricing = $pri->msetupfee . ' ' . $curr->code . '/初装费 ' . $pri->monthly . ' ' . $curr->code . '/月';
        } else {
            $pricing = $pri->monthly . ' ' . $curr->code . '/月';
        }
    }

    $groupname = Capsule::table('tblproductgroups')->where('id', $vars['gid'])->get()[0]->name;

    $text = '<strong>系统通知</strong>' . PHP_EOL . '#上新 #更新 #补货' . PHP_EOL . '<strong>' . $groupname . '/' . $vars['name'] . '</strong> ' . $sc . ' ' . $pricing . PHP_EOL  . $vars['description'] . PHP_EOL  . '<a href="' . TelegramBot_GetSystemURL() . 'cart.php?a=add&pid=' . $vars['pid'] . '">【立即购买】</a>';

    $result =  TelegramBot_send(
        $config['token'],
        "sendMessage",
        [
            "chat_id" =>  $config['channel_chatid'],
            "text" => $text,
            "parse_mode" => "HTML",
        ]
    );

    $json = json_decode($result, true);
    if ($config['stock_pin']) {
        TelegramBot_send(
            $config['token'],
            "pinChatMessage",
            [
                "chat_id" =>   $config['channel_chatid'],
                "message_id" =>  $json['result']['message_id'],
            ]
        );
    }
});

add_hook("AfterCronJob", 1, function ($vars) {
    TelegramBot_Stock_Upgrade();
});

add_hook('DailyCronJob', 1, function ($vars) {
    TelegramBot_Stock_DailyCron();

    $tickets = Capsule::table("tbltickets")->where("status", "Closed")->where("updated_at", "<", date("Y-m-d H:i:s"))->get();
    foreach ($tickets as $ticket) {
        localAPI("DeleteTicket", ["ticketid" => $ticket->id]);
    }
});

add_hook('AfterModuleTerminate', 1, function ($vars) {
    $config = TelegramBot_GetConfigModule();
    if (!$config['auto_add_stock']) {
        return;
    }

    if (php_sapi_name() !== 'cli') {
        return;
    }

    $p = Capsule::table('tblproducts')->where('id', $vars['params']['pid'])->first();
    if ($p->stockcontrol == 1) {
        Capsule::table('tblproducts')->where('id', $vars['params']['pid'])->update(['qty' => $p->qty + 1]);
    }
    Capsule::table('tblhosting')->where('id', $vars['params']['serviceid'])->delete();
    Capsule::table('tblcustomfieldsvalues')->where('relid', $vars['params']['serviceid'])->delete();
});

add_hook('ClientAreaHeaderOutput', 1, function () {
    if (!$_SESSION["uid"]) {
        return;
    }

    if (in_array(basename($_SERVER["SCRIPT_NAME"]), ["index.php"])) {
        return;
    }

    $config = TelegramBot_GetConfigModule();
    if (!$config['force_veify']) {
        return;
    }

    $telegram_id = TelegramBot_GetTelegramUID($_SESSION['uid']);

    if ($telegram_id == false) {
        return  '<script type="text/javascript">alert("请先绑定Telegram");</script>';
    }


    if (!$config['force_subscribe']) {
        return;
    }

    if ($_SESSION['telegram']['subscribe']) {
        return;
    }

    $result = TelegramBot_send(
        $config['token'],
        "getChatMember",
        [
            "chat_id" =>  '@' . $config['subscribe_chatid'],
            "user_id" => $telegram_id,
        ]
    );
    $json = json_decode($result, true);

    $subscribed = $json['ok'] && $json['result']['status'] != 'left';
    if ($subscribed) {
        $_SESSION['telegram']['subscribe'] = true;
        return;
    } else {
        return '<script type="text/javascript">alert("请先关注Telegram通知频道");</script>';
    }
});

add_hook("ClientAreaPage", 1, function () {
    if (!$_SESSION["uid"]) {
        return;
    }

    if (in_array(basename($_SERVER["SCRIPT_NAME"]), ["index.php"])) {
        return;
    }

    $config = TelegramBot_GetConfigModule();
    if (!$config['force_veify']) {
        return;
    }

    $telegram_id = TelegramBot_GetTelegramUID($_SESSION['uid']);

    if ($telegram_id == false) {
        header("Location: /index.php?m=TelegramBot");
        return;
    }

    if (!$config['force_subscribe']) {
        return;
    }

    if ($_SESSION['telegram']['subscribe']) {
        return;
    }

    $result = TelegramBot_send(
        $config['token'],
        "getChatMember",
        [
            "chat_id" =>  '@' . $config['subscribe_chatid'],
            "user_id" => $telegram_id,
        ]
    );
    $json = json_decode($result, true);
    $subscribed = $json['ok'] && $json['result']['status'] != 'left';

    if ($subscribed) {
        $_SESSION['telegram']['subscribe'] = true;
        return;
    } else {
        header("Location: /index.php?m=TelegramBot&action=subscribe");
        return;
    }
});

add_hook("ClientDelete", 1, function ($vars) {
    Capsule::table('mod_TelegramBot_User')->where('user_id', $vars['userid'])->delete();
});

add_hook('EmailPreLog', 1, function ($vars) {
    $config = TelegramBot_GetConfigModule();
    if (!$config['send_client']) {
        return;
    }

    $user = Capsule::table('mod_TelegramBot_User')->where('user_id', $vars['userid']);
    if (!$user->exists()) {
        return;
    }

    $telegram_id = $user->first()->telegram_id;
    TelegramBot_send(
        $config['token'],
        "sendMessage",
        [
            "chat_id" => $telegram_id,
            "text" => '*' . $vars['subject'] . '*' . PHP_EOL . PHP_EOL  . TelegramBot_ParseHtml($vars['message']),
            "parse_mode" => "Markdown",
        ]
    );
});

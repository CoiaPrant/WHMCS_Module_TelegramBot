<?php
require_once(__DIR__ . '/func.php');

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    exit('Unsupport requset method.');
}

$data = file_get_contents('php://input');
$params = json_decode($data, true);

if ($params['message']['date'] > strtotime('-120 seconds')) {
    $GLOBALS['telegram']['skip_admin_reply_hook'] = true;

    logModuleCall('TelegramBot', 'WebHook', $data, 'success');
    TelegramBot_MessageTask($params);
}

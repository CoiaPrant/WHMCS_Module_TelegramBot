<?php
require_once(__DIR__ . '/func.php');

use WHMCS\Database\Capsule;

function TelegramBot_config()
{
    $configarray = array(
        "name" => "Telegram Bot",
        "description" => "",
        "version" => "1.0",
        "author" => "ZeroTime Team",
        "fields" => array(
            "username" => array("FriendlyName" => "用户名", "Type" => "text", "Description" => "Bot的用户名"),
            "token" => array("FriendlyName" => "Token", "Type" => "text", "Description" => "Bot的Token"),
            "force_veify" => array("FriendlyName" => '强制验证', "Type" => 'yesno', "Description" => "强制要求客户验证绑定Telegram"),
            "send_client" => array("FriendlyName" => '消息转发', "Type" => 'yesno', "Description" => "通过Telegram向客户发送相关邮件信息"),

            "force_subscribe" => array("FriendlyName" => '强制订阅频道开关', "Type" => 'yesno'),
            "subscribe_chatid" => array("FriendlyName" => '强制订阅频道用户名', "Type" => 'text', 'Description' => '例如 @zerocloud'),
            "enable_channel" => array("FriendlyName" => '通知开关', "Type" => 'yesno'),
            "channel_chatid" => array("FriendlyName" => '通知频道ID', "Type" => 'text'),

            "stock_updater" => array("FriendlyName" => "库存动态更新", "Type" => "yesno"),
            "stock_mid" => array(
                "FriendlyName" => "动态库存消息ID", "Type" => "text", "Size" => "25",
                "Description" => "用于编辑消息, 可留空"
            ),

            "auto_add_stock" => array("FriendlyName" => "产品到期自动释放库存", "Type" => "yesno"),
            "stock_daily" => array("FriendlyName" => "每日库存通知", "Type" => "yesno"),
            "stock_mid_daily" => array(
                "FriendlyName" => "库存通知消息ID", "Type" => "text", "Size" => "25",
                "Description" => "用于自动删除消息, 可留空"
            ),
            "stock_hook" => array("FriendlyName" => "补货通知开关", "Type" => "yesno", "Description" => "通过Hook自动发送管理员手动补货通知, 隐藏或库存为0时不会发送",),
            "stock_pin" => array(
                "FriendlyName" => "Hook置顶开关", "Type" => "yesno",
                "Description" => "自动置顶Hook发送的消息",
            ),

            "ticket" => array("FriendlyName" => "工单通知开关", "Type" => "yesno"),
            "ticket_chatid" => array("FriendlyName" => "工单通知会话编号", "Type" => "text", "Description" => "需要开启Topic并添加BOT为管理"),
            "ticket_auto" => array("FriendlyName" => "自动回复开关", "Type" => "yesno"),
            "ticket_automsg" => array("FriendlyName" => "工单自动回复内容", "Type" => "textarea"),

            "timeout" => array("FriendlyName" => "超时设定", "Type" => "text", "Description" => "秒", "Default" => "600",),
        ),
    );
    return $configarray;
}

function TelegramBot_activate()
{
    Capsule::schema()->create(
        'mod_TelegramBot_User',
        function ($table) {
            $table->increments('id');
            $table->biginteger('telegram_id')->unique();
            $table->integer('user_id');
        }
    );
    Capsule::schema()->create(
        'mod_TelegramBot_Ticket',
        function ($table) {
            $table->increments('id');
            $table->integer('ticket_id');
            $table->integer('topic_id');
            $table->boolean("is_closed");
        }
    );
}

function TelegramBot_deactivate()
{
    Capsule::schema()->dropIfExists('mod_TelegramBot_User');
    Capsule::schema()->dropIfExists('mod_TelegramBot_Ticket');
}

function TelegramBot_clientarea($params)
{
    if (!isset($_SESSION["uid"])) {
        if (isset($_REQUEST["hash"])) {
            unset($_REQUEST['action']);
            unset($_REQUEST['m']);

            if (!TelegramBot_Authorization($params["token"], $_REQUEST)) {
                return [
                    'pagetitle' => "Telegram登录",
                    'templatefile' => 'templates/clientarea/login-failed.tpl',
                    'vars' => [
                        'params' => $params,
                    ],
                ];
            }


            $client_id = TelegramBot_GetAccount($_REQUEST['id']);
            if ($client_id == false) {
                return [
                    'pagetitle' => "Telegram登录",
                    'templatefile' => 'templates/clientarea/login-nobind.tpl',
                    'vars' => [
                        'params' => $params,
                    ],
                ];
            }

            $client = WHMCS\User\Client::where("id", $client_id)->where("status", "!=", "Closed")->first();
            if ($client == null) {
                return [
                    'pagetitle' => "Telegram登录",
                    'templatefile' => 'templates/clientarea/login-failed.tpl',
                    'vars' => [
                        'params' => $params,
                    ],
                ];
            }

            $user_id = Capsule::table("tblusers_clients")->where("client_id", $client->id)->where("owner", 1)->first()->auth_user_id;
            $user = WHMCS\User\User::where("id", $user_id)->first();

            $auth = new WHMCS\Authentication\Client($client->email, "");
            $auth->setUser($client);
            $auth->finalizeLogin();

            $authmanager = new \WHMCS\Authentication\AuthManager;
            $authmanager->login($user);
            header("Location: clientarea.php");
            exit;
        }

        return [
            'pagetitle' => "Telegram登录",
            'templatefile' => 'templates/clientarea/oauth.tpl',
            'vars' => [
                'params' => $params,
            ],
        ];
    }

    $telegram_id = TelegramBot_GetTelegramUID($_SESSION['uid']);

    switch ($_REQUEST['action']) {
        case 'bind':
            if (isset($_REQUEST["hash"])) {
                unset($_REQUEST['action']);
                unset($_REQUEST['m']);

                if (!TelegramBot_Authorization($params["token"], $_REQUEST)) {
                    return [
                        'pagetitle' => "Telegram 绑定",
                        'templatefile' => 'templates/clientarea/bind-failed.tpl',
                        'vars' => [
                            'params' => $params,
                        ],
                    ];
                }

                Capsule::table('mod_TelegramBot_User')->where('telegram_id', $_REQUEST['id'])->delete();
                if (Capsule::table('mod_TelegramBot_User')->where('user_id', $_SESSION['uid'])->exists()) {
                    Capsule::table('mod_TelegramBot_User')->where('user_id', $_SESSION['uid'])->update(["telegram_id" => (int)$_REQUEST['id']]);
                } else {
                    Capsule::table('mod_TelegramBot_User')->insert(["user_id" => (int)$_SESSION['uid'], "telegram_id" => (int)$_REQUEST['id']]);
                }

                return [
                    'pagetitle' => 'Telegram 绑定',
                    'templatefile' => 'templates/clientarea/bind-success.tpl',
                    'requirelogin' => true,
                    'vars' => [
                        'params' => $params,
                        'telegram_id' => $_REQUEST['id'],
                    ],
                ];
            }

            return [
                'pagetitle' => 'Telegram 绑定',
                'templatefile' => 'templates/clientarea/bind.tpl',
                'requirelogin' => true,
                'vars' => [
                    'params' => $params,
                ],
            ];

            break;
        case "subscribe":
            return [
                'pagetitle' => 'Telegram',
                'templatefile' => 'templates/clientarea/subscribe.tpl',
                'requirelogin' => true,
                'vars' => [
                    'params' => $params,
                    'telegram_id' => $telegram_id,
                ],
            ];
        default:
            return [
                'pagetitle' => 'Telegram绑定',
                'templatefile' => 'templates/clientarea/view.tpl',
                'requirelogin' => true,
                'vars' => [
                    'params' => $params,
                    'telegram_id' => $telegram_id,
                ],
            ];
    }
}

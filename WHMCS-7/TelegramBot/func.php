<?php
require_once(__DIR__ . '/../../../init.php');
require_once(__DIR__ . "/vendor/autoload.php");

use WHMCS\Database\Capsule;
use League\HTMLToMarkdown\HtmlConverter;

/*
全局变量
*/

$config = TelegramBot_GetConfigModule();
set_time_limit($config['timeout']);

/*
模块命令
*/

function TelegramBot_MessageTask(array $params)
{
    if (!isset($params["message"])) {
        return;
    }

    global $config;

    if ($params["message"]["chat"]["id"] == $config["ticket_chatid"]) {
        if (!isset($params["message"]["message_thread_id"])) {
            return;
        }

        if (isset($params["message"]["forum_topic_created"])) {
            return;
        } else if (
            isset($params["message"]["pinned_message"]) ||
            isset($params["message"]["forum_topic_edited"]) || isset($params["message"]["forum_topic_reopened"]) || isset($params["message"]["forum_topic_closed"])
        ) {
            TelegramBot_send(
                $config['token'],
                "deleteMessage",
                [
                    "chat_id" => $params['message']['chat']['id'],
                    "message_id" => $params["message"]["message_id"],
                ]
            );
        }

        $ticket = Capsule::table("mod_TelegramBot_Ticket")->where("topic_id", $params["message"]["message_thread_id"])->first();
        if ($ticket == null || (bool)$ticket->is_closed) {
            return;
        }
        if (
            isset($params["message"]["pinned_message"]) ||
            isset($params["message"]["forum_topic_created"]) || isset($params["message"]["forum_topic_edited"]) || isset($params["message"]["forum_topic_reopened"])
        ) {
            return;
        } else if (isset($params["message"]["forum_topic_closed"])) {
            localAPI("DeleteTicket", ['ticketid' => $ticket->ticket_id, "status" => "Closed"]);
            return;
        } else if (isset($params["message"]["text"])) {
            localAPI("AddTicketReply", [
                'ticketid' => $ticket->ticket_id,
                'adminusername' => trim($params['message']['from']['first_name'] . ' ' . $params['message']['from']['last_name']),
                'message' => $params['message']['text'],
                'markdown' => false,
            ]);
            return;
        } else if (isset($params["message"]["animation"])) {
            $result = TelegramBot_send(
                $config['token'],
                "getFile",
                [
                    "file_id" => $params['message']['animation']['file_id'],
                ]
            );

            $json = json_decode($result, true);
            if (!$json["ok"]) {
                return;
            }

            localAPI("AddTicketReply", [
                'ticketid' => $ticket->ticket_id,
                'adminusername' => trim($params['message']['from']['first_name'] . ' ' . $params['message']['from']['last_name']),
                'message' => $params['message']['caption'] ?? " ",
                'attachments' => base64_encode(json_encode([
                    [
                        'name' => $params['message']['animation']['file_name'],
                        'data' => base64_encode(TelegramBot_getFile($config['token'], $json['result']['file_path'])),
                    ],
                ])),
                'markdown' => false,
            ]);
            return;
        } else if (isset($params["message"]["audio"])) {
            $result = TelegramBot_send(
                $config['token'],
                "getFile",
                [
                    "file_id" => $params['message']['audio']['file_id'],
                ]
            );


            $json = json_decode($result, true);
            if (!$json["ok"]) {
                return;
            }

            localAPI("AddTicketReply", [
                'ticketid' => $ticket->ticket_id,
                'adminusername' => trim($params['message']['from']['first_name'] . ' ' . $params['message']['from']['last_name']),
                'message' => $params['message']['caption'] ?? " ",
                'attachments' => base64_encode(json_encode([
                    [
                        'name' => $params['message']['audio']['file_name'],
                        'data' => base64_encode(TelegramBot_getFile($config['token'], $json['result']['file_path'])),
                    ],
                ])),
                'markdown' => false,
            ]);
            return;
        } else if (isset($params["message"]["document"])) {
            $result = TelegramBot_send(
                $config['token'],
                "getFile",
                [
                    "file_id" => $params['message']['document']['file_id'],
                ]
            );

            $json = json_decode($result, true);
            if (!$json["ok"]) {
                return;
            }

            localAPI("AddTicketReply", [
                'ticketid' => $ticket->ticket_id,
                'adminusername' => trim($params['message']['from']['first_name'] . ' ' . $params['message']['from']['last_name']),
                'message' => $params['message']['caption'] ?? " ",
                'attachments' => base64_encode(json_encode([
                    [
                        'name' => $params['message']['document']['file_name'],
                        'data' => base64_encode(TelegramBot_getFile($config['token'], $json['result']['file_path'])),
                    ],
                ])),
                'markdown' => false,
            ]);
            return;
        } else if (isset($params["message"]["sticker"])) {
            $result = TelegramBot_send(
                $config['token'],
                "getFile",
                [
                    "file_id" => $params['message']['sticker']['file_id'],
                ]
            );

            $json = json_decode($result, true);
            if (!$json["ok"]) {
                return;
            }

            $sticker = TelegramBot_getSticker($config['token'], $json['result']['file_path']);

            localAPI("AddTicketReply", [
                'ticketid' => $ticket->ticket_id,
                'adminusername' => trim($params['message']['from']['first_name'] . ' ' . $params['message']['from']['last_name']),
                'message' => $params['message']['caption'] ?? " ",
                'attachments' => base64_encode(json_encode([
                    [
                        'name' => $sticker['name'],
                        'data' => base64_encode($sticker['data']),
                    ],
                ])),
                'markdown' => false,
            ]);
            return;
        } else if (isset($params["message"]["photo"])) {
            $result = TelegramBot_send(
                $config['token'],
                "getFile",
                [
                    "file_id" => $params['message']['photo'][count($params['message']['photo']) - 1]['file_id'],
                ]
            );

            $json = json_decode($result, true);
            if (!$result["ok"]) {
                return;
            }

            localAPI("AddTicketReply", [
                'ticketid' => $ticket->ticket_id,
                'adminusername' => trim($params['message']['from']['first_name'] . ' ' . $params['message']['from']['last_name']),
                'message' => $params['message']['caption'] ?? " ",
                'attachments' => base64_encode(json_encode([
                    [
                        'name' => basename($json['result']['file_path']),
                        'data' => base64_encode(TelegramBot_getFile($config['token'], $json['result']['file_path'])),
                    ],
                ])),
                'markdown' => false,
            ]);
            return;
        } else if (isset($params["message"]["video"])) {
            $result = TelegramBot_send(
                $config['token'],
                "getFile",
                [
                    "file_id" => $params['message']['video']['file_id'],
                ]
            );

            $json = json_decode($result, true);
            if (!$json["ok"]) {
                return;
            }

            localAPI("AddTicketReply", [
                'ticketid' => $ticket->ticket_id,
                'adminusername' => trim($params['message']['from']['first_name'] . ' ' . $params['message']['from']['last_name']),
                'message' => $params['message']['caption'] ?? " ",
                'attachments' => base64_encode(json_encode([
                    [
                        'name' => $params['message']['video']['file_name'],
                        'data' => base64_encode(TelegramBot_getFile($config['token'], $json['result']['file_path'])),
                    ],
                ])),
                'markdown' => false,
            ]);
            return;
        } else if (isset($params["message"]["video_note"])) {
            $result = TelegramBot_send(
                $config['token'],
                "getFile",
                [
                    "file_id" => $params['message']['video_note']['file_id'],
                ]
            );

            $json = json_decode($result, true);
            if (!$json["ok"]) {
                return;
            }

            localAPI("AddTicketReply", [
                'ticketid' => $ticket->ticket_id,
                'adminusername' => trim($params['message']['from']['first_name'] . ' ' . $params['message']['from']['last_name']),
                'message' => $params['message']['caption'] ?? " ",
                'attachments' => base64_encode(json_encode([
                    [
                        'name' => $params['message']['video_note']['file_name'],
                        'data' => base64_encode(TelegramBot_getFile($config['token'], $json['result']['file_path'])),
                    ],
                ])),
                'markdown' => false,
            ]);
            return;
        } else if (isset($params["message"]["voice"])) {
            $result = TelegramBot_send(
                $config['token'],
                "getFile",
                [
                    "file_id" => $params['message']['voice']['file_id'],
                ]
            );

            $json = json_decode($result, true);
            if (!$json["ok"]) {
                return;
            }

            localAPI("AddTicketReply", [
                'ticketid' => $ticket->ticket_id,
                'adminusername' => trim($params['message']['from']['first_name'] . ' ' . $params['message']['from']['last_name']),
                'message' => $params['message']['caption'] ?? " ",
                'attachments' => base64_encode(json_encode([
                    [
                        'name' => $params['message']['voice']['file_name'],
                        'data' => base64_encode(TelegramBot_getFile($config['token'], $json['result']['file_path'])),
                    ],
                ])),
                'markdown' => false,
            ]);
            return;
        } else {
            TelegramBot_send(
                $config['token'],
                "sendMessage",
                [
                    "chat_id" => $params['message']['chat']['id'],
                    "message_thread_id" => $params["message"]["message_thread_id"],
                    "text" => "Unsupport message.",
                ]
            );
            return;
        }
        return;
    }

    $command = strtolower(str_replace('@' . $config['username'], '', $params['message']['text']));
    $user_id = TelegramBot_GetAccount($params['message']['from']['id']);

    if ($params['message']['chat']['type'] != 'private') {
        return;
    }

    switch ($command) {
        case  "/pwreset":
            if ($user_id == false) {
                TelegramBot_send(
                    $config['token'],
                    "sendMessage",
                    [
                        "chat_id" => $params['message']['chat']['id'],
                        "text" => '您未绑定帐号，请前往<a href="' . TelegramBot_GetSystemURL() . 'index.php?m=TelegramBot">这里</a>进行绑定',
                        "parse_mode" => "HTML",
                    ]
                );
                return;
            }

            $entity = \WHMCS\User\Client::where("id", $user_id)->where("status", "!=", "Closed")->first();
            $resetKey = hash("sha256", $entity->id . mt_rand(100000, 999999) . $entity->passwordHash);
            $entity->passwordResetKey = $resetKey;
            $entity->passwordResetKeyExpiryDate = \WHMCS\Carbon::now()->addHours(2);
            $entity->save();
            $resetUrl = fqdnRoutePath("password-reset-use-key", $resetKey);

            TelegramBot_send(
                $config['token'],
                "sendMessage",
                [
                    "chat_id" => $params['message']['chat']['id'],
                    "text" => '<a href="' . $resetUrl . '">密码重置链接</a>' . PHP_EOL . '有效期2小时, 请尽快使用',
                    "parse_mode" => "HTML",
                ]
            );
            return;
        case '/uid':
            TelegramBot_send(
                $config['token'],
                "sendMessage",
                [
                    "chat_id" => $params['message']['chat']['id'],
                    "text" => $params['message']['message_id'], "你的Telegram UID是 <pre>" . $params['message']['from']['id'] . '</pre>',
                    "parse_mode" => "HTML",
                ]
            );
            return;
        case '/help':
        case '/start':
            TelegramBot_send(
                $config['token'],
                "sendMessage",
                [
                    "chat_id" => $params['message']['chat']['id'],
                    "text" => "<strong>命令菜单</strong>" . PHP_EOL . "/uid 查询Telegram UID" . PHP_EOL . "/pwreset 重置用户密码",
                    "parse_mode" => "HTML",
                ]
            );
            return;
    }
}

/*
   模块函数
*/

function TelegramBot_GetAccount(int $telegram_id)
{
    if (Capsule::table('mod_TelegramBot_User')->where('telegram_id', $telegram_id)->exists()) {
        return Capsule::table('mod_TelegramBot_User')->where('telegram_id', $telegram_id)->first()->user_id;
    } else {
        return false;
    }
}

function TelegramBot_GetTelegramUID(int $user_id)
{
    if (Capsule::table('mod_TelegramBot_User')->where('user_id', $user_id)->exists()) {
        return Capsule::table('mod_TelegramBot_User')->where('user_id', $user_id)->first()->telegram_id;
    } else {
        return false;
    }
}

function TelegramBot_send(string $token, string $endpoint, array $params, array $files = null)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_HEADER, 0);

    curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/bot' . $token . '/' . $endpoint);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);

    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
    if ($files == null) {
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json ']);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
    } else {
        $boundary = uniqid();
        $delimiter = '-------------' . $boundary;
        $data = build_data_files($boundary, $params, $files);

        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: multipart/form-data; boundary=' . $delimiter,
            "Content-Length: " . strlen($data),
        ]);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    $output = curl_exec($curl);
    curl_close($curl);

    logModuleCall('TelegramBot', $endpoint, $params, $output);
    return $output;
}

function TelegramBot_downloadFile(string $token, string $file_path)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_HEADER, 0);

    curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/file/bot' . $token . '/' . $file_path);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);

    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    $output = curl_exec($curl);
    curl_close($curl);

    logModuleCall('TelegramBot', "downloadFile", ["file_path" => $file_path], base64_encode($output));
    return $output;
}

if (!function_exists("build_data_files")) {
    function build_data_files($boundary, $fields, $files)
    {
        $eol = "\r\n";
        $data = '';

        $delimiter = '-------------' . $boundary;

        foreach ($fields as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . "\"" . $eol . $eol
                . $content . $eol;
        }

        foreach ($files as $name => $file) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $file["name"] . '"' . $eol
                . "Content-Type: application/octet-stream" . $eol;

            $data .= $eol;
            $data .= $file['content'] . $eol;
        }
        $data .= "--" . $delimiter . "--" . $eol;

        return $data;
    }
}

function TelegramBot_Authorization($token, $auth_data)
{
    $check_hash = $auth_data['hash'];
    unset($auth_data['hash']);
    $data_check_arr = [];
    foreach ($auth_data as $key => $value) {
        $data_check_arr[] = $key . '=' . $value;
    }
    sort($data_check_arr);
    $data_check_string = implode("\n", $data_check_arr);
    $secret_key = hash('sha256', $token, true);
    $hash = hash_hmac('sha256', $data_check_string, $secret_key);

    if (strcmp($hash, $check_hash) !== 0) {
        return false;
    }

    if ((time() - $auth_data['auth_date']) > 86400) {
        return false;
    }
    return true;
}

/*
WHMCS 相关函数
*/

function TelegramBot_GetAdminPath()
{
    global $customadminpath;

    if (isset($customadminpath) && !empty($customadminpath)) {
        return $customadminpath;
    }

    return 'admin';
}

function TelegramBot_GetConfigModule()
{
    $AddonModuleConfig = Capsule::table('tbladdonmodules')->where('module', '=', 'TelegramBot')->get();
    $config            = [];

    for ($i = 0; $i < count($AddonModuleConfig); $i++) //var_dump($vars);
    {
        $config[$AddonModuleConfig[$i]->setting] = $AddonModuleConfig[$i]->value;
    }

    return $config;
}

function TelegramBot_GetSystemURL()
{
    return rtrim(Capsule::table('tblconfiguration')->where('setting', '=', 'SystemURL')->first()->value, '/') . '/';
}

function TelegramBot_ParseHtml($str)
{
    $str = preg_replace("/<style .*?<\\/style>/is", "", $str);
    $str = preg_replace("/<a .*?<img .*?\\/>/is", "", $str);

    $converter = new HtmlConverter();
    $str = trim(strip_tags($converter->convert($str)));

    $str = str_replace(" ", " ", $str);
    $str = str_replace("\r", "", $str);

    $lines = explode("\n", $str);
    foreach ($lines as $i => $line) {
        $lines[$i] = trim($line);
    }
    $str = implode("\n", $lines);

    while (is_int(strpos($str, "  ")) ||  is_int(strpos($str, "\n\n"))) {
        $str = str_replace("  ", " ", $str);
        $str = str_replace("\n\n", "\n", $str);
    }

    return htmlspecialchars_decode($str);
}

/*
库存函数
*/

function TelegramBot_Stock_Upgrade()
{
    $config = TelegramBot_GetConfigModule();
    if (!$config['stock_updater'] || !$config['enable_channel']) {
        return;
    }

    if (Capsule::table('tblproductgroups')->where('hidden', '!=', 1)->count() < 1) {
        return;
    }

    $stocks = [];

    $groups = [];
    foreach (Capsule::table('tblproductgroups')->where('hidden', '!=', 1)->get() as $group) {
        $stocks[$group->id] = [];
        $groups[$group->id] = $group;
    }
    usort($groups, function ($a, $b) {
        return $a->order < $b->order ? -1 : 1;
    });

    $products = [];
    foreach (Capsule::table('tblproducts')->get() as $product) {
        $products[$product->id] = $product;

        if ($product->hidden == 1) {
            continue;
        }

        $stocks[$product->gid][] = ['type' => 'product', 'data' => $product, 'extra' => []];
    }

    foreach (Capsule::table('tblbundles')->where('validuntil', '<=', date('Y-m-d'))->where('showgroup', '!=', 0)->where(function ($sql) {
        $sql->where('validuntil', '>', date('Y-m-d'))->orWhere('validuntil', '0000-00-00');
    })->get() as $bundle) {
        if ($bundle->maxuses > 0 && $bundle->uses >= $bundle->maxuses) {
            continue;
        }

        $extra = [];
        $extra['stockcontrol'] = $bundle->maxuses > 0 ? 1 : 0;

        if ($bundle->maxuses == 0) {
            $extra['qty'] = 0;
        } else {
            $extra['qty'] = $bundle->maxuses - $bundle->uses;
        }

        $items = unserialize($bundle->itemdata);
        if (is_array($items)) {
            foreach ($items as $item) {
                if ($item['type'] != 'product') {
                    continue;
                }

                $product = $products[(int)$item['pid']];
                if ($product == null) {
                    continue;
                }

                if ($product->stockcontrol == 1) {
                    $extra['stockcontrol'] = 1;

                    if ($product->qty <= 0) {
                        $extra['qty'] = 0;
                        break;
                    }

                    if ($product->qty < $extra['qty'] || $extra['qty'] == 0) {
                        $extra['qty'] = $product->qty;
                        continue;
                    }
                }
            }
        }

        $stocks[$bundle->gid][] = ['type' => 'bundle', 'data' => $bundle, 'extra' => $extra];
    }

    $text = '库存动态更新' . PHP_EOL . '购买前请确认<strong>商品名称</strong>与<strong>注意事项~</strong>' . PHP_EOL . PHP_EOL;

    foreach ($groups as $group) {
        $text .= '<strong><a href="' . TelegramBot_GetSystemURL() . 'cart.php?gid=' . $group->id . '">' . $group->name . '</a></strong>' . PHP_EOL;

        if (count($stocks[$group->id]) <= 0) {
            $text .=  '无' . PHP_EOL;
            continue;
        }

        usort($stocks[$group->id], function ($a, $b) {
            $orderA = 0;
            switch ($a['type']) {
                case 'product':
                    $orderA = $a['data']->order;
                    break;
                case 'bundle':
                    $orderA = $a['data']->sortorder;
                    break;
                default:
                    return 0;
            }

            $orderB = 0;
            switch ($b['type']) {
                case 'product':
                    $orderB = $b['data']->order;
                    break;
                case 'bundle':
                    $orderB = $b['data']->sortorder;
                    break;
                default:
                    return 0;
            }

            return $orderA < $orderB ? -1 : 1;
        });

        foreach ($stocks[$group->id] as $stock) {
            switch ($stock['type']) {
                case 'product':
                    $product = $stock['data'];

                    if ($product->stockcontrol == 0) {
                        $text .=  '[库存: 大量] <a href="' . TelegramBot_GetSystemURL() . 'cart.php?a=add&pid=' . $product->id . '">' . $product->name . '</a>' . PHP_EOL;
                    } else if ($product->qty > 0) {
                        $text .=  '[库存: ' . $product->qty . '] <a href="' . TelegramBot_GetSystemURL() . 'cart.php?a=add&pid=' . $product->id . '">' . $product->name . '</a>' . PHP_EOL;
                    } else {
                        $text .=  '[库存: 0] ' . $product->name . PHP_EOL;
                    }
                    break;
                case 'bundle':
                    $bundle = $stock['data'];
                    $extra = $stock['extra'];

                    if ($extra['stockcontrol'] == 0) {
                        $text .=  '[库存: 大量] <a href="' . TelegramBot_GetSystemURL() . 'cart.php?a=add&bid=' . $bundle->id . '">' . $bundle->name . '</a>' . PHP_EOL;
                    } else if ($extra['qty'] > 0) {
                        $text .=  '[库存: ' . $extra['qty'] . '] <a href="' . TelegramBot_GetSystemURL() . 'cart.php?a=add&bid=' . $bundle->id . '">' . $bundle->name . '</a>' . PHP_EOL;
                    } else {
                        $text .=  '[库存: 0] ' . $bundle->name . PHP_EOL;
                    }
                    break;
                default:
            }
        }
    }

    $text .=  PHP_EOL . '更新时间: <strong>' . date('Y-m-d H:i:s') . '</strong>';

    if (empty($config['stock_mid'])) {
        $result =  TelegramBot_send(
            $config['token'],
            "sendMessage",
            [
                "chat_id" => $config['channel_chatid'],
                "text" => $text,
                "parse_mode" => "HTML",
            ]
        );
        $json = json_decode($result, true);
        if ($json['ok']) {
            Capsule::table('tbladdonmodules')->where('module', 'TelegramBot')->where('setting', 'stock_mid')->update(['value' => $json['result']['message_id']]);
        }
    } else {
        TelegramBot_send(
            $config['token'],
            "editMessageText",
            [
                "chat_id" => $config['channel_chatid'],
                "message_id" => $config['stock_mid'],
                "text" => $text,
                "parse_mode" => "HTML",
            ]
        );
    }
}

function TelegramBot_Stock_DailyCron()
{
    $config = TelegramBot_GetConfigModule();
    if (!$config['stock_daily'] || !$config['enable_channel']) {
        return;
    }

    $stocks = [];

    $groups = [];
    foreach (Capsule::table('tblproductgroups')->where('hidden', '!=', 1)->get() as $group) {
        $stocks[$group->id] = [];
        $groups[$group->id] = $group;
    }
    usort($groups, function ($a, $b) {
        return $a->order < $b->order ? -1 : 1;
    });

    $products = [];
    foreach (Capsule::table('tblproducts')->get() as $product) {
        $products[$product->id] = $product;

        if ($product->hidden == 1 || $product->stockcontrol == 0 || $product->qty <= 0) {
            continue;
        }

        $stocks[$product->gid][] = ['type' => 'product', 'data' => $product, 'extra' => []];
    }

    foreach (Capsule::table('tblbundles')->where('validuntil', '<=', date('Y-m-d'))->where('showgroup', '!=', 0)->where(function ($sql) {
        $sql->where('validuntil', '>', date('Y-m-d'))->orWhere('validuntil', '0000-00-00');
    })->get() as $bundle) {
        if ($bundle->maxuses > 0 && $bundle->uses >= $bundle->maxuses) {
            continue;
        }

        $extra = [];
        $extra['stockcontrol'] = $bundle->maxuses > 0 ? 1 : 0;

        if ($bundle->maxuses == 0) {
            $extra['qty'] = 0;
        } else {
            $extra['qty'] = $bundle->maxuses - $bundle->uses;
        }

        $items = unserialize($bundle->itemdata);
        if (is_array($items)) {
            foreach ($items as $item) {
                if ($item['type'] != 'product') {
                    continue;
                }

                $product = $products[(int)$item['pid']];
                if ($product == null) {
                    continue;
                }

                if ($product->stockcontrol == 1) {
                    $extra['stockcontrol'] = 1;

                    if ($product->qty <= 0) {
                        $extra['qty'] = 0;
                        break;
                    }

                    if ($product->qty < $extra['qty'] || $extra['qty'] == 0) {
                        $extra['qty'] = $product->qty;
                        continue;
                    }
                }
            }
        }

        if ($extra['stockcontrol'] == 0 || $extra['qty'] <= 0) {
            continue;
        }

        $stocks[$bundle->gid][] = ['type' => 'bundle', 'data' => $bundle, 'extra' => $extra];
    }

    $send = false;
    $text = '补货通知' . PHP_EOL . '购买前请确认<strong>商品名称</strong>与<strong>注意事项~</strong>' . PHP_EOL . PHP_EOL;
    foreach ($groups as $group) {
        if (count($stocks[$group->id]) <= 0) {
            continue;
        }

        $send = true;
        $text .= '<strong><a href="' . TelegramBot_GetSystemURL() . 'cart.php?gid=' . $group->id . '">' . $group->name . '</a></strong>' . PHP_EOL;
        usort($stocks[$group->id], function ($a, $b) {
            $orderA = 0;
            switch ($a['type']) {
                case 'product':
                    $orderA = $a['data']->order;
                    break;
                case 'bundle':
                    $orderA = $a['data']->sortorder;
                    break;
                default:
                    return 0;
            }

            $orderB = 0;
            switch ($b['type']) {
                case 'product':
                    $orderB = $b['data']->order;
                    break;
                case 'bundle':
                    $orderB = $b['data']->sortorder;
                    break;
                default:
                    return 0;
            }

            return $orderA < $orderB ? -1 : 1;
        });

        foreach ($stocks[$group->id] as $stock) {
            switch ($stock['type']) {
                case 'product':
                    $product = $stock['data'];

                    $text .=  '[库存: ' . $product->qty . '] <a href="' . TelegramBot_GetSystemURL() . 'cart.php?a=add&pid=' . $product->id . '">' . $product->name . '</a>' . PHP_EOL;
                    break;
                case 'bundle':
                    $bundle = $stock['data'];
                    $extra = $stock['extra'];

                    $text .=  '[库存: ' . $extra['qty'] . '] <a href="' . TelegramBot_GetSystemURL() . 'cart.php?a=add&bid=' . $bundle->id . '">' . $bundle->name . '</a>' . PHP_EOL;
                    break;
                default:
            }
        }
    }

    if (!empty($config['stock_mid_daily'])) {
        TelegramBot_send(
            $config['token'],
            "deleteMessage",
            [
                "chat_id" => $config['channel_chatid'],
                "message_id" => $config['stock_mid_daily'],
            ]
        );
        Capsule::table('tbladdonmodules')->where('module', 'TelegramBot')->where('setting', 'stock_mid_daily')->update(['value' => '']);
    }

    if (!$send) {
        return;
    }

    $result = TelegramBot_send(
        $config['token'],
        "sendMessage",
        [
            "chat_id" => $config['channel_chatid'],
            "text" => $text,
            "parse_mode" => "HTML",
        ]
    );

    $json = json_decode($result, true);
    if ($json['ok']) {
        Capsule::table('tbladdonmodules')->where('module', 'TelegramBot')->where('setting', 'stock_mid_daily')->update(['value' => $json['result']['message_id']]);
    }
}

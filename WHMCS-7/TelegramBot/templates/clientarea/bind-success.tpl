{include file='./head.tpl'}

<div class="col-sm-6 col-sm-offset-3">
	<div class="alert alert-success">
		<strong>绑定成功!</strong>
		<br>
		新的Telegram UserID: {$telegram_id}
	</div>
</div>

<div class="col-sm-6 col-sm-offset-3">
	<a class="btn btn-primary" href="clientarea.php">
		返回
	</a>
</div>

{include file='./foot.tpl'}